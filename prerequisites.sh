#!/bin/bash

set -eu

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

# Configure /bin/sh as bash
echo "dash dash/sh boolean false" | debconf-set-selections
dpkg-reconfigure dash

dpkg --add-architecture i386
apt-get update --yes --quiet
apt-get upgrade --yes --quiet

# Adapted from the list of dependencies in PetaLinux Release Note.
# Some packages are not required but are nice to have inside dev container:
# bash-completion, sudo, vim

apt-get install --yes --quiet --no-install-recommends \
    autoconf \
    automake \
    bash-completion \
    bc \
    bison \
    build-essential \
    chrpath \
    cpio \
    debianutils \
    diffstat \
    flex \
    gawk \
    gcc-multilib \
    git \
    gnupg \
    gzip \
    iproute2 \
    iputils-ping \
    less \
    libegl1-mesa \
    libglib2.0-dev \
    libncurses5-dev \
    libsdl1.2-dev \
    libselinux1 \
    libssl-dev \
    libtinfo5 \
    libtool \
    locales \
    net-tools \
    pax \
    pylint3 \
    python \
    python3 \
    python3-git \
    python3-jinja2 \
    python3-pip \
    python3-pexpect \
    rsync \
    screen \
    socat \
    sudo \
    tar \
    texinfo \
    tftpd-hpa \
    unzip \
    vim \
    wget \
    xterm \
    xxd \
    xz-utils \
    zlib1g-dev \
    zlib1g:i386

apt-get clean
rm -rf var/lib/apt/lists/*

localedef --inputfile=en_US --force --charmap=UTF-8 --alias-file=/usr/share/locale/locale.alias en_US.UTF-8
