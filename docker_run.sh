#!/bin/bash

docker run \
    --interactive \
    --tty \
    --rm \
    --name "$(id -u)_${RANDOM}" \
    --env "DISPLAY=${DISPLAY}" \
    --env H_USERNAME="$(id -u -n)" \
    --env H_GROUPNAME="$(id -g -n)" \
    --env H_UID="$(id -u)" \
    --env H_GID="$(id -g)" \
    --env H_HOME="${HOME}" \
    --volume /tmp/.X11-unix:/tmp/.X11-unix \
    --volume "${HOME}:${HOME}" \
    "$@"
