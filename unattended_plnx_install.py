#!/usr/bin/env python3


import sys
import pexpect


def main():
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} <PetaLinux install command>", file=sys.stderr)
        sys.exit(1)

    try:
        child = pexpect.spawn(sys.argv[1], sys.argv[2:], timeout=None,
                              encoding="utf-8", logfile=sys.stdout)

        child.expect("Press Enter to display the license agreements", timeout=600)
        child.sendline("")

        # Exit the pager (showing EULAs and T&C) and say yes to all questions
        question_num = 0
        while question_num < 3:
            index = child.expect([pexpect.TIMEOUT, "\\[y/N] > "], timeout=0.3)
            if index == 0:
                child.send("q")
            else:
                child.sendline("y")
                question_num += 1

        child.expect("PetaLinux SDK has been installed")
        child.expect(pexpect.EOF)

    except pexpect.exceptions.TIMEOUT:
        child.close(force=True)
        print(f"{sys.argv[0]}: Installation timed-out", file=sys.stderr)
        sys.exit(2)

    except pexpect.exceptions.EOF:
        print(f"{sys.argv[0]}: Installation ended unexpectedly", file=sys.stderr)
        sys.exit(2)

    except Exception as e:
        # TODO converting exception to string may cause character decoding
        # error on some system and masks the original exception.
        print(f"{sys.argv[0]}: {e}", file=sys.stderr)
        sys.exit(2)

    print("Unattended installation finished successfully.")


if __name__ == "__main__":
    main()
