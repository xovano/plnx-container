# Prepare installation environment for PetaLinux
FROM ubuntu:20.04 AS plnx-prereqs
COPY prerequisites.sh .
RUN ./prerequisites.sh && rm ./prerequisites.sh
ENV LANG=en_US.UTF-8


# Slim image needs an externally-mounted PetaLinux installation
FROM plnx-prereqs AS plnx-slim
ARG PLNX_USERNAME=plnx
ARG PLNX_GROUPNAME=plnx
ARG PLNX_UID=1000
ARG PLNX_GID=1000

RUN groupadd -g "${PLNX_GID}" "${PLNX_GROUPNAME}" \
    && useradd -u "${PLNX_UID}" -g "${PLNX_GID}" -s /bin/bash -m "${PLNX_USERNAME}" \
    && usermod -aG sudo "${PLNX_USERNAME}" \
    && echo "${PLNX_USERNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER "${PLNX_USERNAME}"
WORKDIR "/home/${PLNX_USERNAME}"

ENV DEFAULT_PLNX_DIR=/opt/xilinx/petalinux
COPY source_plnx.sh /
CMD [ "/bin/bash" ]
ENTRYPOINT [ "/source_plnx.sh" ]


# Full image contains a PetaLinux installation
FROM plnx-slim AS plnx-full
ARG PLNX_INSTALLER=petalinux-installer.run
ARG PLNX_DIR="${DEFAULT_PLNX_DIR}"
ARG PLNX_PLATFORMS=aarch64

# Make entrypoint script happy
ENV PLNX_DIR="${PLNX_DIR}"

COPY "${PLNX_INSTALLER}" unattended_plnx_install.py ./
RUN sudo mkdir -p "${PLNX_DIR}" \
    && sudo chown "$(id -u)" "${PLNX_DIR}" \
    && ./unattended_plnx_install.py "./$(basename ${PLNX_INSTALLER})" -d "${PLNX_DIR}" -p "${PLNX_PLATFORMS}" \
    && rm unattended_plnx_install.py "$(basename ${PLNX_INSTALLER})" petalinux_installation_log
