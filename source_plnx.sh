#!/bin/bash

set -e

if [ -z "${PLNX_DIR}" ]; then
    echo "Warning: PLNX_DIR is not set, using default value ${DEFAULT_PLNX_DIR}"
    PLNX_DIR="${DEFAULT_PLNX_DIR}"
fi

PLNX_SCRIPT="${PLNX_DIR}/settings.sh"

# Wrapper function to prevent arguments of this script from being passed to ${PLNX_SCRIPT}
do_source() {
    # shellcheck source=/dev/null
    source "${PLNX_SCRIPT}"
}

if [ -r "${PLNX_SCRIPT}" ]; then
    if [ $# -ne 0 ]; then
        do_source
        exec "$@"
    else
        echo "Error: need at least 1 argument" >&2
    fi
else
    echo "Error: ${PLNX_SCRIPT} does not exist or is not readable" >&2
fi
